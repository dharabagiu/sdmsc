import PIL.Image as I, PIL.ImageDraw as ID, sys as S, numpy as N
img = I.open(S.argv[1])
pixels = img.load()
points = [(i,j) for (i,j) in N.ndindex(img.size) if pixels[i,j]==(0,0,0,255)]
midx, midy = sum([p[0] for p in points]) / len(points), sum([p[1] for p in points]) / len(points)
a = sum([(p[0] - midx) * (p[1] - midy) for p in points]) / sum([(p[0] - midx)**2 for p in points])
b = midy - a * midx
ID.Draw(img).line((0, b, img.size[0], a * img.size[0] + b), fill='red')
img.save(S.argv[2])

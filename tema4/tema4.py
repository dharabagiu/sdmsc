import os
import re
import sqlite3
import pickle
from sys import argv
from hashlib import sha256
from tema3 import pe_info, jaccard


sql_create_table = """ CREATE TABLE IF NOT EXISTS Homeworks (
                               Hash    text PRIMARY KEY,
                               Assign  integer NOT NULL,
                               Student integer NOT NULL,
                               Ngrams  blob NOT NULL
                           ); """

sql_insert_homework = """ INSERT INTO
                          Homeworks(Hash, Assign, Student, Ngrams)
                          VALUES(?, ?, ?, ?); """


def build_features_raw(binaries_path, db_path):
    os.unlink(db_path)
    conn = sqlite3.connect(db_path)
    conn.execute(sql_create_table)
    re_filename = re.compile('a([0-9]+)_s([0-9]+)\\.exe')
    check_name = lambda f : os.path.isfile(os.path.join(binaries_path, f)) and re_filename.match(f)
    files = [f for f in os.listdir(binaries_path) if check_name(f)]
    cur = conn.cursor()
    for f in files:
        path = os.path.join(binaries_path, f)
        with open(path, 'rb') as fd:
            h = sha256(fd.read()).hexdigest()
        m = re_filename.match(f)
        assign = int(m.group(1))
        student = int(m.group(2))
        ngrams = sqlite3.Binary(pickle.dumps(pe_info(path)['ngrams_5']))
        cur.execute(sql_insert_homework, (h, assign, student, ngrams))
    cur.close()
    conn.commit()
    conn.close()


def build_features(binaries_path, db_path):
    os.unlink(db_path)
    conn = sqlite3.connect(db_path)
    conn.execute(sql_create_table)
    re_filename = re.compile('a([0-9]+)_s([0-9]+)\\.exe')
    check_name = lambda f : os.path.isfile(os.path.join(binaries_path, f)) and re_filename.match(f)
    files = [f for f in os.listdir(binaries_path) if check_name(f)]
    cur = conn.cursor()
    ngrams_all = dict()
    ngrams_freq = dict()
    for f in files:
        path = os.path.join(binaries_path, f)
        ngrams = pe_info(path)['ngrams_5']
        ngrams_all[f] = ngrams
        for n in ngrams:
            if n in ngrams_freq:
                ngrams_freq[n] += 1
            else:
                ngrams_freq[n] = 1
    for f in files:
        path = os.path.join(binaries_path, f)
        with open(path, 'rb') as fd:
            h = sha256(fd.read()).hexdigest()
        m = re_filename.match(f)
        assign = int(m.group(1))
        student = int(m.group(2))
        ng = set()
        for n in ngrams_all[f]:
            if ngrams_freq[n] <= 30:
                ng.add(n)
        ngrams = sqlite3.Binary(pickle.dumps(ng))
        cur.execute(sql_insert_homework, (h, assign, student, ngrams))
    cur.close()
    conn.commit()
    conn.close()


def sim1(db, h1, h2):
    sql_select_homework = "SELECT ngrams FROM Homeworks WHERE Hash = ? OR Hash = ?;"
    conn = sqlite3.connect(db)
    cur = conn.cursor()
    cur.execute(sql_select_homework, (h1, h2))
    rows = cur.fetchall()
    if len(rows) != 2:
        return 0
    ngrams1 = pickle.loads(rows[0][0])
    ngrams2 = pickle.loads(rows[1][0])
    cur.close()
    conn.close()
    return jaccard(ngrams1, ngrams2)


def sim2(db, assign, s1, s2):
    sql_select_homework = """ SELECT ngrams FROM Homeworks WHERE
                              Assign = ? AND (Student = ? OR Student = ?);"""
    conn = sqlite3.connect(db)
    cur = conn.cursor()
    cur.execute(sql_select_homework, (assign, s1, s2))
    rows = cur.fetchall()
    if len(rows) != 2:
        return 0
    ngrams1 = pickle.loads(rows[0][0])
    ngrams2 = pickle.loads(rows[1][0])
    cur.close()
    conn.close()
    return jaccard(ngrams1, ngrams2)


def topsim(db):
    sql_select_assign = "SELECT DISTINCT Assign FROM Homeworks;"
    sql_select_homeworks = "SELECT Student, Ngrams FROM Homeworks WHERE Assign = ?;"
    conn = sqlite3.connect(db)
    cur = conn.cursor()
    cur.execute(sql_select_assign)
    assignments = [r[0] for r in cur.fetchall()]
    assignments.sort()
    similarities = list()
    for assign in assignments:
        cur.execute(sql_select_homeworks, (assign,))
        rows = cur.fetchall()
        for i, h1 in enumerate(rows):
            for h2 in rows[i + 1:]:
                if h1[0] != h2[0]:
                    ngrams1 = pickle.loads(h1[1])
                    ngrams2 = pickle.loads(h2[1])
                    similarities.append((assign, h1[0], h2[0], jaccard(ngrams1, ngrams2)))
    similarities.sort(key=lambda h: h[3], reverse=True)
    cur.close()
    conn.close()
    print('    |  A |   S1   S2 |     SIM')
    print('------------------------------')
    for i, s in enumerate(similarities[:500]):
        print('%3d | %02d | %04d %04d | %6.2f%%' % (i + 1, s[0], s[1], s[2], s[3] * 100))


def main():
    if len(argv) >= 2:
        if argv[1] == 'generate-db':
            build_features_raw('./homeworks/binaries', 'features_raw.db')
            build_features('./homeworks/binaries', 'features.db')
        elif argv[1] == 'top':
            topsim('features.db')
        elif argv[1] == 'top-raw':
            topsim('features_raw.db')


if __name__ == '__main__':
    main()

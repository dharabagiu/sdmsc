import pefile
import distorm3
from binascii import crc32
from sys import argv


def pe_info(file_path):
    info = dict()

    pe = pefile.PE(file_path, fast_load=True)

    machine = pe.FILE_HEADER.Machine
    if machine == 0x14c:
        info['platform'] = 'x86'
    elif machine == 0x200:
        info['platform'] = 'ia64'
    elif machine == 0x8664:
        info['platform'] = 'x86-64'
    else:
        info['platform'] = 'unknown'

    info['number_of_sections'] = pe.FILE_HEADER.NumberOfSections

    scn_exec_mask = pefile.SECTION_CHARACTERISTICS['IMAGE_SCN_MEM_EXECUTE']
    entry_point = pe.OPTIONAL_HEADER.AddressOfEntryPoint
    executable_scns = list()
    for i, section in enumerate(pe.sections):
        if section.Characteristics & scn_exec_mask:
            executable_scns.append(i)
            
    info['number_of_sections_exec'] = len(executable_scns)

    for i in executable_scns:
        section = pe.sections[i]
        va = section.VirtualAddress
        vs = section.Misc_VirtualSize
        if entry_point >= va and entry_point < va + vs:
            info['entry_point_section'] = i
            info['entry_point_offset'] = (entry_point - va) / vs
            break

    opcodes = list()
    for i in executable_scns:
        section = pe.sections[i]
        if machine == 0x14c:
            dec_type = distorm3.Decode32Bits
        else:
            dec_type = distorm3.Decode64Bits
        va = section.VirtualAddress
        vs = section.Misc_VirtualSize
        code = distorm3.Decode(0, pe.get_memory_mapped_image()[va:va+vs], dec_type)
        for instr in code:
            disasm = instr[2]
            if 'INT 3' not in disasm and 'NOP' not in disasm:
                opcode = disasm.split()[0]
                if opcode in ['ADD', 'SUB', 'INC', 'DEC', 'NEG', 'ADC', 'SBB']:
                    opcode = 'C_ARITH'
                elif opcode in ['AND', 'OR', 'XOR', 'NOT']:
                    opcode = 'C_LOGIC'
                elif opcode in ['MUL', 'IMUL', 'DIV', 'IDIV']:
                    opcode = 'C_ARTH2'
                elif opcode in ['CMP', 'TEST']:
                    opcode = 'C_COMPR'
                elif opcode in ['SHL', 'SHR', 'SAL', 'SAR', 'SHLD', 'SHRD', 'ROR', 'ROL', 'RCR', 'RCL']:
                    opcode = 'C_SHIFT'
                elif opcode in ['JE', 'JZ', 'JNE', 'JNZ', 'JG', 'JNLE', 'JGE', 'JNL', 'JL', 'JNGE']:
                    opcode = 'C_CJUMP'
                elif opcode in ['JLE', 'JNG', 'JA', 'JNBE', 'JAE', 'JNB', 'JB', 'JNAE', 'JBE', 'JNA']:
                    opcode = 'C_CJUMP'
                elif opcode in ['JXCZ', 'JC', 'JNC', 'JO', 'JNO', 'JP', 'JPE', 'JNP', 'JPO', 'JS', 'JNS']:
                    opcode = 'C_CJUMP'
                elif opcode in ['LOOP', 'LOOPE', 'LOOPZ', 'LOOPNE', 'LOOPNZ']:
                    opcode = 'C_LOOPS'
                opcodes.append(opcode)

    ngrams = set()
    for i in range(len(opcodes)):
        concat = ''.join(opcodes[i : min(i+5, len(opcodes))])
        ngrams.add(crc32(concat.encode('utf8')))

    info['ngrams_5'] = ngrams

    pe.close()
    return info


def pe_jaccard(pe_info1, pe_info2):
    ngrams1 = pe_info1['ngrams_5']
    ngrams2 = pe_info2['ngrams_5']
    ci = 0
    for ng in ngrams1:
        if ng in ngrams2:
            ci = ci + 1
    cr = len(ngrams1) + len(ngrams2) - ci
    return ci / cr

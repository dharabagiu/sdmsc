from tema3 import pe_info, pe_jaccard
from sys import argv

print(pe_jaccard(pe_info(argv[1]), pe_info(argv[2])))

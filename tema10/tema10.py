from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn import svm
from numpy import arange
import matplotlib.pyplot as plt


test_size = 0.1
random_state = 42


def load_dataset():
    digits = load_digits()
    return digits.data, digits.target


def decompose(n, X, x):
    pca = PCA(n_components=n)
    pca.fit(X)
    return pca.transform(x)


def split(x, y, train_size):
    return train_test_split(x, y, train_size=train_size, random_state=random_state, stratify=y)


def predict(x, y, x_predict):
    svc = svm.SVC(kernel='linear')
    svc.fit(x, y)
    return svc.predict(x_predict)


def get_accuracy(y1, y2):
    matches = list(map(lambda i: int(y1[i] == y2[i]), range(len(y1))))
    return sum(matches) / len(matches)


if __name__ == '__main__':
    x, y = load_dataset()

    print('Dataset size:', len(x))
    print('Number of traits:', len(x[0]))

    x_train, x_test, y_train, y_test = split(x, y, 1 - test_size)

    n = 1
    while n <= len(x[0]):
        x_train_dec = decompose(n, x, x_train)
        x_test_dec = decompose(n, x, x_test)

        predict_accuracy = []
        train_predict_accuracy = []

        train_sizes = arange(0.1, 1 - test_size + 0.1, 0.1)

        for ts in [x / (1 - test_size) for x in train_sizes]:
            if ts == 1.0:
                x_train_spl, y_train_spl = x_train_dec, y_train
            else:
                x_train_spl, _, y_train_spl, _ = split(x_train_dec, y_train, ts)
            y_predict = predict(x_train_spl, y_train_spl, x_test_dec)
            predict_accuracy.append(get_accuracy(y_test, y_predict))
            y_predict = predict(x_train_spl, y_train_spl, x_train_spl)
            train_predict_accuracy.append(get_accuracy(y_train_spl, y_predict))

        plt.clf()
        plt.plot(train_sizes, predict_accuracy)
        plt.plot(train_sizes, train_predict_accuracy)
        plt.xlabel('Train size')
        plt.ylabel('Accuracy')
        plt.title('Number of components = %s' % (n,))
        plt.legend(['test', 'train'])
        plt.savefig('%s.png' % (n,))

        n *= 2

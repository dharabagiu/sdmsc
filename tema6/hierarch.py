from PIL import Image, ImageDraw
from random import randrange
from heapq import heappush, heappop
from math import sqrt, ceil
import os


def dist_heap(points):
    h = list()
    for i in range(len(points) - 1):
        for j in range(i + 1, len(points)):
            dist = (points[i][0] - points[j][0])**2 % 131072 + (points[i][1] - points[j][1])**2 % 131072
            heappush(h, (dist, i, j))
    return h


def hierarch_clustering(img):
    pixels = img.load()

    points = list()
    for i in range(0, img.size[0]):
        for j in range(0, img.size[1]):
            if (img.mode == 'P' and pixels[i,j] == 0) or (img.mode == 'RGBA' and pixels[i,j] == (0, 0, 0, 255)):
                points.append((i, j))

    dendr = dict()
    dendr[0] = [i for i in range(len(points))]
    dist = dist_heap(points)
    last_dist = 0

    while True:
        if len(dist) == 0:
            break
        min_dist = heappop(dist)
        clusters = dendr[last_dist]
        oldc = clusters[min_dist[2]]
        newc = clusters[min_dist[1]]
        if oldc == newc:
            continue
        #print('Replacing %d with %d' % (oldc, newc))
        if min_dist[0] == last_dist:
            new_clusters = clusters
        else:
            new_clusters = clusters.copy()
            #print("New dist: ", sqrt(min_dist[0]))
        #print('Clusters before replacement:', set(new_clusters))
        for i in range(len(new_clusters)):
            if new_clusters[i] == oldc:
                new_clusters[i] = newc
        #print('Clusters after replacement:', set(new_clusters))
        dendr[min_dist[0]] = new_clusters
        last_dist = min_dist[0]
    
    return points, dendr


def make_img(points, dendr, dist, width, height):
    print('Making image for distance', sqrt(dist))
    img = Image.new('RGB', (width, height), color='white')
    pixels = img.load()
    if dist not in dendr:
        distances = list(dendr.keys())
        distances.sort()
        for i, d in enumerate(distances):
            if d == dist:
                break
            elif d > dist:
                dist = distances[i - 1]
                break
            if i == len(distances) - 1:
                dist = distances[i]
    clusters = dendr[dist]
    clusters2 = dict()
    for p, c in enumerate(clusters):
        if c in clusters2:
            clusters2[c].append(p)
        else:
            clusters2[c] = [p]
    colors = dict()
    print('Number of clusters:', len(clusters2))
    for c in clusters2.keys():
        colors[c] = (randrange(0, 256), randrange(0, 256), randrange(0, 256))
    for c, ps in clusters2.items():
        for p in ps:
            pixels[points[p][0], points[p][1]] = colors[c]
    return img
    

if __name__ == '__main__':
    src_dir = 'dataset'
    dst_dir = 'hierarch'
    if not os.path.exists(dst_dir):
        os.mkdir(dst_dir)
    spacing = 10
    for file in os.listdir(src_dir):
        print('Processing %s...' % (file,))
        path = os.path.join(src_dir, file)
        img = Image.open(path)
        points, dendr = hierarch_clustering(img)
        samples_count = len(dendr) - 1
        cols = 6
        rows = ceil(samples_count / cols)
        res = Image.new('RGB', ((img.size[0] + spacing) * cols - spacing, (img.size[1] + spacing) * rows), color='black')
        distances = list(dendr.keys())[1:]
        d = ImageDraw.Draw(res)
        for r in range(rows):
            for c in range(cols):
                idx = r * cols + c
                if idx >= len(distances):
                    break
                img2 = make_img(points, dendr, distances[idx], img.size[0], img.size[1])
                res.paste(img2, (c * (img.size[0] + spacing), spacing + r * (img.size[1] + spacing)))
                dist = sqrt(distances[idx])
                d.text((c * (img.size[0] + spacing), r * (img.size[1] + spacing)), 'dist = %.2f' % (dist,), fill=(255, 255, 255))
        dst_path = os.path.join(dst_dir, file)
        res.save(dst_path)

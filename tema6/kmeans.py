from PIL import Image, ImageDraw
from random import randrange
import os


def kmeans(img, num_clusters):
    pixels = img.load()

    points = list()
    for i in range(0, img.size[0]):
        for j in range(0, img.size[1]):
            if (img.mode == 'P' and pixels[i,j] == 0) or (img.mode == 'RGBA' and pixels[i,j] == (0, 0, 0, 255)):
                points.append((i, j))

    centroids = [points[randrange(0, len(points))] for _ in range(num_clusters)]

    stop = False
    while not stop:
        clusters = [list() for _ in range(num_clusters)]
        for p in points:
            i = p[0]
            j = p[1]
            min_dist = -1
            min_k = 0
            for k in range(0, num_clusters):
                dist = pow(i - centroids[k][0], 2) + pow(j - centroids[k][1], 2)
                if min_dist < 0 or dist < min_dist:
                    min_dist = dist
                    min_k = k
            clusters[min_k].append((i, j))

        centroids2 = list()
        for k in range(len(centroids)):
            mid = [0, 0]
            for p in clusters[k]:
                mid[0] += p[0]
                mid[1] += p[1]
            mid[0] /= len(clusters[k])
            mid[1] /= len(clusters[k])
            min_dist = -1
            min_p = [0, 0]
            for p in clusters[k]:
                dist = pow(p[0] - mid[0], 2) + pow(p[1] - mid[1], 2)
                if min_dist < 0 or dist < min_dist:
                    min_dist = dist
                    min_p = p
            centroids2.append(tuple(min_p))

        stop = True
        for k in range(len(centroids)):
            if abs(centroids[k][0] - centroids2[k][0]) > 0:
                stop = False
                break
            if abs(centroids[k][1] - centroids2[k][1]) > 0:
                stop = False
                break
        centroids = centroids2
    
    colors = [(randrange(0, 256), randrange(0, 256), randrange(0, 256)) for _ in range(num_clusters)]
    img2 = Image.new('RGB', img.size, color='white')
    pixels2 = img2.load()

    for k, elements in enumerate(clusters):
        for p in elements:
            pixels2[p[0],p[1]] = colors[k]

    return img2


if __name__ == '__main__':
    src_dir = 'dataset'
    dst_dir = 'kmeans'
    if not os.path.exists(dst_dir):
        os.mkdir(dst_dir)
    ks = [[2, 3, 5], [10, 15, 20]]
    spacing = 10
    for file in os.listdir(src_dir):
        print('Processing %s...' % (file,))
        path = os.path.join(src_dir, file)
        img = Image.open(path)    
        res = Image.new('RGB', ((img.size[0] + spacing) * len(ks[0]) - spacing, (img.size[1] + spacing) * len(ks)), color='black')
        d = ImageDraw.Draw(res)
        for i, row in enumerate(ks):
            for j, k in enumerate(row):
                img2 = kmeans(img, k)
                res.paste(img2, (j * (img.size[0] + spacing), spacing + i * (img.size[1] + spacing)))
                d.text((j * (img.size[0] + spacing), i * (img.size[1] + spacing)), 'k = %d' % (k,), fill=(255, 255, 255))
        dst_path = os.path.join(dst_dir, file)
        res.save(dst_path)

from PIL import Image, ImageDraw
from random import randrange
from heapq import heappush, heappop
from math import sqrt, ceil
import os
import time


def dist_heap(points, delta=10):
    delta2 = delta**2
    cells = dict()

    cellmaxx = 0
    cellmaxy = 0

    for i in range(len(points)):
        p = points[i]
        cellx = p[0] // delta
        celly = p[1] // delta
        if cellx > cellmaxx:
            cellmaxx = cellx
        if celly > cellmaxy:
            cellmaxy = celly
        if (cellx, celly) in cells:
            cells[(cellx, celly)].append(i)
        else:
            cells[(cellx, celly)] = [i]

    h = list()

    def compute_distances(mypoint_index, candidates):
        for i in candidates:
            mypoint = points[mypoint_index]
            p = points[i]
            dist = (mypoint[0] - p[0])**2 + (mypoint[1] - p[1])**2
            if dist <= delta2:
                heappush(h, (dist, mypoint_index, i))

    for celly in range(0, cellmaxy + 1):
        for cellx in range(0, cellmaxx + 1):
            if (cellx, celly) not in cells:
                continue
            c = cells[(cellx, celly)]
            v1 = cells.get((cellx + 1, celly), [])
            v2 = cells.get((cellx - 1, celly + 1), [])
            v3 = cells.get((cellx, celly + 1), [])
            v4 = cells.get((cellx + 1, celly + 1), [])
            for i in range(len(c)):
                compute_distances(c[i], c[i + 1:])
                compute_distances(c[i], v1)
                compute_distances(c[i], v2)
                compute_distances(c[i], v3)
                compute_distances(c[i], v4)

    return h


class DSNode:
    def __init__(self, data):
        self.parent = self
        self.rank = 0
        self.size = 1
        self.data = data


def DSFind(x):
    root = x
    while root.parent is not root:
        root = root.parent
    while x.parent is not root:
        parent = x.parent
        x.parent = root
        x = parent
    return root


def DSUnion(x, y):
    xRoot = DSFind(x)
    yRoot = DSFind(y)
    if xRoot is yRoot:
        return False
    if xRoot.rank < yRoot.rank:
        xRoot, yRoot = yRoot, xRoot
    yRoot.parent = xRoot
    if xRoot.rank == yRoot.rank:
        xRoot.rank += 1
    return True


def hierarch_clustering(img):
    pixels = img.load()

    points = list()
    for i in range(0, img.size[0]):
        for j in range(0, img.size[1]):
            if (img.mode == 'P' and pixels[i,j] == 0) or (img.mode == 'RGBA' and pixels[i,j] == (0, 0, 0, 255)):
                points.append((i, j))

    ds = [DSNode(i) for i in range(len(points))]
    distances = dist_heap(points)
    last_dist = 0
    dendr = dict()

    def add_entry():
        entry = []
        for node in ds:
            entry.append(DSFind(node).data)
        dendr[last_dist] = entry


    while len(distances) != 0:
        min_dist = heappop(distances)
        if DSFind(ds[min_dist[1]]) is DSFind(ds[min_dist[2]]):
            continue
        if last_dist != min_dist[0]:
            add_entry()
            last_dist = min_dist[0]
        DSUnion(ds[min_dist[1]], ds[min_dist[2]])
    add_entry()
    
    return points, dendr


def make_img(points, dendr, dist, width, height):
    img = Image.new('RGB', (width, height), color='white')
    pixels = img.load()
    if dist not in dendr:
        distances = list(dendr.keys())
        distances.sort()
        for i, d in enumerate(distances):
            if d == dist:
                break
            elif d > dist:
                dist = distances[i - 1]
                break
            if i == len(distances) - 1:
                dist = distances[i]
    clusters = dendr[dist]
    clusters2 = dict()
    for p, c in enumerate(clusters):
        if c in clusters2:
            clusters2[c].append(p)
        else:
            clusters2[c] = [p]
    colors = dict()
    for c in clusters2.keys():
        colors[c] = (randrange(0, 256), randrange(0, 256), randrange(0, 256))
    for c, ps in clusters2.items():
        for p in ps:
            pixels[points[p][0], points[p][1]] = colors[c]
    return img
    

if __name__ == '__main__':
    start_time = time.time()

    src_dir = 'dataset'
    dst_dir = 'hierarch'
    if not os.path.exists(dst_dir):
        os.mkdir(dst_dir)
    spacing = 10
    for file in os.listdir(src_dir):
        print('Processing %s...' % (file,))
        path = os.path.join(src_dir, file)
        img = Image.open(path)
        points, dendr = hierarch_clustering(img)
        samples_count = len(dendr) - 1
        cols = 6
        rows = ceil(samples_count / cols)
        res = Image.new('RGB', ((img.size[0] + spacing) * cols - spacing, (img.size[1] + spacing) * rows), color='black')
        distances = list(dendr.keys())[1:]
        d = ImageDraw.Draw(res)
        for r in range(rows):
            for c in range(cols):
                idx = r * cols + c
                if idx >= len(distances):
                    break
                img2 = make_img(points, dendr, distances[idx], img.size[0], img.size[1])
                res.paste(img2, (c * (img.size[0] + spacing), spacing + r * (img.size[1] + spacing)))
                dist = sqrt(distances[idx])
                d.text((c * (img.size[0] + spacing), r * (img.size[1] + spacing)), 'dist = %.2f' % (dist,), fill=(255, 255, 255))
        dst_path = os.path.join(dst_dir, file)
        res.save(dst_path)

    end_time = time.time()
    print('Elapsed time:', end_time - start_time)

import json
import math
import re
import numpy


def calculate_tweet_scores(twitter_data, word_scores):
    scores = dict()
    splitter = re.compile('[\\s.,\\?:;]+')
    for tweet in twitter_data:
        scores[tweet['id']] = 0
        words = [w.lower() for w in splitter.split(tweet['text'])]
        for i in range(0, len(words)):
            if words[i] in word_scores:
                scores[tweet['id']] += word_scores[words[i]]
            elif i != len(words) - 1:
                dword = words[i].join(words[i + 1])
                if dword in word_scores:
                    scores[tweet['id']] += word_scores[dword]
    return scores


def word_frequency(twitter_data, top=500):
    freq = dict()
    pattern = re.compile('^[a-zA-Z\\\'][a-zA-Z\\-\\\']*$')
    splitter = re.compile('[\\s.,\\?:;]+')
    for tweet in twitter_data:
        for word in [w.lower() for w in splitter.split(tweet['text'])]:
            if pattern.match(word) and word not in ['rt', 'https']:
                if word not in freq:
                    freq[word] = 1
                else:
                    freq[word] += 1
    return {k:v for k, v in sorted(freq.items(), key=lambda kv: kv[1], reverse=True)[:top]}


def derive_word_scores(twitter_data, word_scores, tweet_scores, new_words):
    def floor2(num):
        if num < 0:
            return math.ceil(num)
        else:
            return math.floor(num)

    pattern = re.compile('^[a-zA-Z\\\'][a-zA-Z\\-\\\']*$')
    splitter = re.compile('[\\s.,\\?:;]+')
    new_scores = dict()
    new_words_fr = dict()
    for word in new_words:
        if word not in word_scores:
            new_scores[word] = 0
            new_words_fr[word] = 0
    for tweet in twitter_data:
        for word in [w.lower() for w in splitter.split(tweet['text'])]:
            if pattern.match(word) and word not in ['rt', 'https'] and word in new_scores:
                new_scores[word] += tweet_scores[tweet['id']]
                new_words_fr[word] += 1
    new_scores = {k: (lambda w, s: s / new_words_fr[w])(k, v) for (k, v) in new_scores.items()}
    norm_score = abs(word_scores[max(word_scores, key=lambda k: abs(word_scores[k]))])
    norm_new_score = abs(new_scores[max(new_scores, key=lambda k: abs(new_scores[k]))])
    new_scores = {k: (lambda s: floor2(s / norm_new_score * norm_score))(v) for (k, v) in new_scores.items()}
    return new_scores


def friends_and_happiness(twitter_data, tweet_scores):
    happiness = dict()
    tweet_count = dict()
    friends_count = dict()
    for tweet in twitter_data:
        user_id = tweet['user']['id']
        if user_id not in happiness:
            happiness[user_id] = 0
        if user_id not in tweet_count:
            tweet_count[user_id] = 0
        happiness[user_id] += tweet_scores[tweet['id']]
        tweet_count[user_id] += 1
        if user_id not in friends_count:
            friends_count[user_id] = tweet['user']['friends_count']
    for k, v in happiness.items():
        happiness[k] /= tweet_count[k]
    
    x = list()
    x.append(list())
    x.append(list())
    for v in happiness.values():
        x[0].append(v)
    for v in friends_count.values():
        x[1].append(v)
    return numpy.corrcoef(x)[0, 1]


def main():
    twitter_data = list()
    with open('twitter_data1.txt', 'r') as fp:
        for line in fp:
            twitter_data.append(json.loads(line))
    
    word_scores = dict()
    with open('sentiment_scores.txt', 'r') as fp:
        for line in fp:
            spl = line.split()
            word_scores["".join(spl[:len(spl) - 1])] = int(spl[len(spl) - 1])

    tweet_scores = calculate_tweet_scores(twitter_data, word_scores)
    with open('out1.txt', 'w') as fp:
        for k, v in tweet_scores.items():
            fp.write('%d %3d\n' % (k, v))

    frequent_words = word_frequency(twitter_data)
    with open('out2.txt', 'w') as fp:
        for k, v in frequent_words.items():
            fp.write('%-15s %3d\n' % (k, v))

    new_scores = derive_word_scores(twitter_data, word_scores, tweet_scores, [w[0] for w in frequent_words.items()])
    with open('out3.txt', 'w') as fp:
        for k, v in new_scores.items():
            fp.write('%-15s %2d\n' % (k, v))

    correlation = friends_and_happiness(twitter_data, tweet_scores)
    with open('out4.txt', 'w') as fp:
        fp.write(str(correlation))

    
if __name__ == '__main__':
    main()

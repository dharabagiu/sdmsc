import numpy as np
import matplotlib.pyplot as plt
import os


def read_graph(filename):
    file = open(filename, 'r')
    n = int(file.readline())
    graph = []
    for i in range(n):
        row = [int(x) for x in file.readline().split()][:n]
        graph.append(row)
    return graph


def transition_matrix(graph):
    n = len(graph)
    tm = [[0 for _ in range(n)] for _ in range(n)]
    for i in range(n):
        outgoing = sum(graph[i])
        if outgoing == 0:
            continue
        p = 1 / outgoing
        for j in range(n):
            if graph[i][j] == 1:
                tm[j][i] = p
    return tm


def pagerank(graph, save_images=False):
    tm = transition_matrix(graph)
    n = len(graph)
    v = [[1 / n] for _ in range(n)]
    i = 0
    makefig(v, i)
    while True:
        v2 = np.dot(tm, v)
        if np.linalg.norm(np.subtract(v2, v)) < 0.0001:
            return v
        v = v2
        i += 1
        if save_images:
            makefig(v, i)


def makefig(v, step):
    if not os.path.exists('results'):
        os.mkdir('results')
    plt.figure()
    xs = [i for i in range(1, len(v) + 1)]
    plt.bar(xs, [x[0] for x in v])
    plt.xticks(xs)
    plt.savefig(os.path.join('results', str(step) + '.png'))


def main():
    graph = read_graph('graph.txt')
    v = pagerank(graph, True)
    print(v)


if __name__ == '__main__':
    main()

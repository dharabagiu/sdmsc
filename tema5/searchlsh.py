from sys import argv
from random import randrange
from hashlib import sha1
import os
import sqlite3
import pickle
import time


myprime = 1000000000931
num_bands = 30
num_rows = 5


sql_create_table = """ CREATE TABLE IF NOT EXISTS LSH (
                            Hash text NOT NULL,
                            BandIndex integer NOT NULL,
                            BandHash text NOT NULL,
                            PRIMARY KEY (Hash, BandIndex)
                       ); """


def jaccard(ngrams1, ngrams2):
    ci = 0
    for ng in ngrams1:
        if ng in ngrams2:
            ci = ci + 1
    cr = len(ngrams1) + len(ngrams2) - ci
    if cr == 0:
        return 0
    return ci / cr


def sim(h1, h2):
    sql_select_homework = 'SELECT Ngrams FROM Homeworks WHERE Hash = ? OR Hash = ?;'
    connection = sqlite3.connect('features.db')
    cursor = connection.cursor()
    cursor.execute(sql_select_homework, (h1, h2))
    rows = cursor.fetchall()
    if len(rows) != 2:
        return 0
    ngrams1 = pickle.loads(rows[0][0])
    ngrams2 = pickle.loads(rows[1][0])
    cursor.close()
    connection.close()
    return jaccard(ngrams1, ngrams2)


def minhash(A, B, ngrams, band, row):
    a = A[band * num_rows + row]
    b = B[band * num_rows + row]
    perm = [(a * x + b) % myprime for x in ngrams]
    return min(perm)


def compute_bands(A, B, ngrams):
    bands = []
    for band in range(num_bands):
        mh = [minhash(A, B, ngrams, band, row) for row in range(num_rows)]
        bands.append(sha1(pickle.dumps(mh)).hexdigest())
    return bands


def generate_lsh():
    sql_select_all = 'SELECT Hash, Ngrams from Homeworks;'
    sql_insert_lsh = 'INSERT INTO LSH(Hash, BandIndex, BandHash) VALUES(?, ?, ?);'

    A = [randrange(myprime) for i in range(num_bands * num_rows)]
    B = [randrange(myprime) for i in range(num_bands * num_rows)]

    if os.path.exists('minhash.bin'):
        os.unlink('minhash.bin')
    pickle.dump(A + B, open('minhash.bin', 'wb'))
    
    connection = sqlite3.connect('features.db')
    cursor = connection.cursor()
    cursor.execute(sql_select_all)
    collection = []
    for row in cursor:
        collection.append((row[0], pickle.loads(row[1])))
    cursor.close()
    connection.close()

    if os.path.exists('lsh.db'):
        os.unlink('lsh.db')
    connection = sqlite3.connect('lsh.db')
    cursor = connection.cursor()
    cursor.execute(sql_create_table)
    for file, ngrams in collection:
        if len(ngrams) == 0:
            continue
        for band, H in enumerate(compute_bands(A, B, ngrams)):
            cursor.execute(sql_insert_lsh, (file, band, H))

    cursor.close()
    connection.commit()
    connection.close()


def search_lsh(file):
    AB = pickle.load(open('minhash.bin', 'rb'))
    A = AB[:num_bands * num_rows]
    B = AB[num_bands * num_rows:]

    connection = sqlite3.connect('features.db')
    cursor = connection.cursor()

    cursor.execute('SELECT Ngrams FROM Homeworks WHERE Hash = ?;', (file,))
    row = cursor.fetchone()
    if row is not None:
        ngrams = pickle.loads(row[0])
        bands = compute_bands(A, B, ngrams)
        cursor.close()
        connection.close()
        connection = sqlite3.connect('lsh.db')
        cursor = connection.cursor()
        candidates = set()

        for band, H in enumerate(bands):
            cursor.execute('SELECT Hash FROM LSH WHERE BandIndex = ? AND BandHash = ?;', (band, H))
            candidates |= set([row[0] for row in cursor.fetchall()])

        print('Number of candidates: %d' % len(candidates))
        for candidate in candidates:
            s = sim(file, candidate)
            if 1 - s <= 0.3:
                yield (candidate, s)


def search_all_lsh():
    AB = pickle.load(open('minhash.bin', 'rb'))
    A = AB[:num_bands * num_rows]
    B = AB[num_bands * num_rows:]

    connection = sqlite3.connect('features.db')
    cursor = connection.cursor()

    cursor.execute('SELECT Hash, Ngrams FROM Homeworks ORDER BY Hash;')

    connection2 = sqlite3.connect('lsh.db')
    cursor2 = connection2.cursor()

    for row in cursor:
        ngrams = pickle.loads(row[1])
        if len(ngrams) == 0:
            continue
        bands = compute_bands(A, B, ngrams)
        candidates = set()

        for band, H in enumerate(bands):
            cursor2.execute('SELECT Hash FROM LSH WHERE BandIndex = ? AND BandHash = ?;', (band, H))
            candidates |= set([row[0] for row in cursor2.fetchall()])

        printed = False

        candidates_list = list(candidates)
        candidates_list.sort()
        for candidate in candidates_list:
            s = sim(row[0], candidate)
            if 1 - s <= 0.3:
                if not printed:
                    print('Results for %s:' % (row[0],))
                    printed = True
                print('%s    %5.2f%%' % (candidate, s * 100))
        
        if printed:
            print()
    
    cursor.close()
    connection.close()
    cursor2.close()
    connection2.close()


def main():
    if not os.path.exists('features.db'):
        print('Features database not found')
        exit(0)

    
    if not os.path.exists('lsh.db') or not os.path.exists('minhash.bin'):
        generate_lsh()
    
    if len(argv) > 1:
        if argv[1] == 'all':
            start_time = time.time()
            search_all_lsh()
            print('Time spent: %s seconds' % (time.time() - start_time))
        else:
            for match in search_lsh(argv[1]):
                print('%s    %5.2f%%' % (match[0], match[1] * 100))


if __name__ == '__main__':
    main()

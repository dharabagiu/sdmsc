from sys import argv
import os
import sqlite3
import pickle
import dummyMapReduce as mr
import time


sql_create_table = """ CREATE TABLE IF NOT EXISTS Inverted (
                            Ngram   integer NOT NULL,
                            Hash    text NOT NULL,
                            PRIMARY KEY (Ngram, Hash)
                       ); """


class InvertedIndex(mr.MapReduce):
    def __init__(self):
        self.out = {}

    def map(self, file, ngrams):
        for ngram in ngrams:
            self.emit(ngram, file)

    def reduce(self, ngram, files):
        self.out[ngram] = files


def jaccard(ngrams1, ngrams2):
    ci = 0
    for ng in ngrams1:
        if ng in ngrams2:
            ci = ci + 1
    cr = len(ngrams1) + len(ngrams2) - ci
    if cr == 0:
        return 0
    return ci / cr


def sim(h1, h2):
    sql_select_homework = 'SELECT Ngrams FROM Homeworks WHERE Hash = ? OR Hash = ?;'
    connection = sqlite3.connect('features.db')
    cursor = connection.cursor()
    cursor.execute(sql_select_homework, (h1, h2))
    rows = cursor.fetchall()
    if len(rows) != 2:
        return 0
    ngrams1 = pickle.loads(rows[0][0])
    ngrams2 = pickle.loads(rows[1][0])
    cursor.close()
    connection.close()
    return jaccard(ngrams1, ngrams2)


def generate_inv():
    sql_select_all = 'SELECT Hash, Ngrams from Homeworks;'
    sql_insert_inv = 'INSERT INTO Inverted(Ngram, Hash) VALUES(?, ?);'

    ii = InvertedIndex()

    connection = sqlite3.connect('features.db')
    cursor = connection.cursor()
    cursor.execute(sql_select_all)
    collection = []
    for row in cursor:
        collection.append((row[0], pickle.loads(row[1])))
    ii.run(collection)
    cursor.close()
    connection.close()

    if os.path.exists('inverted.db'):
        os.unlink('inverted.db')
    connection = sqlite3.connect('inverted.db')
    cursor = connection.cursor()
    cursor.execute(sql_create_table)
    for ngram, files in ii.out.items():
        for file in files:
            cursor.execute(sql_insert_inv, (ngram, file))

    cursor.close()
    connection.commit()
    connection.close()


def search_inv(file):
    connection = sqlite3.connect('inverted.db')
    cursor = connection.cursor()

    cursor.execute('SELECT Ngram FROM Inverted WHERE Hash = ?;', (file,))
    ngrams = [row[0] for row in cursor]
    candidates = set()
    for ngram in ngrams:
        cursor.execute('SELECT Hash FROM Inverted WHERE Ngram = ?;', (ngram,))
        candidates |= set([row[0] for row in cursor])
    
    print('Number of candidates: %d' % len(candidates))
    for candidate in candidates:
        s = sim(file, candidate)
        if 1 - s <= 0.3:
            yield (candidate, s)


def search_all_inv():
    connection = sqlite3.connect('inverted.db')
    cursor = connection.cursor()

    cursor.execute('SELECT DISTINCT Hash FROM Inverted ORDER BY Hash;')
    rows_files = cursor.fetchall()
    for file_hash in [row[0] for row in rows_files]:
        cursor.execute('SELECT Ngram FROM Inverted WHERE Hash = ?;', (file_hash,))
        ngrams = [row[0] for row in cursor]
        candidates = set()
        for ngram in ngrams:
            cursor.execute('SELECT Hash FROM Inverted WHERE Ngram = ?;', (ngram,))
            candidates |= set([row[0] for row in cursor])

        printed = False

        candidates_list = list(candidates)
        candidates_list.sort()
        for candidate in candidates_list:
            s = sim(file_hash, candidate)
            if 1 - s <= 0.3:
                if not printed:
                    print('Results for %s:' % (file_hash,))
                    printed = True
                print('%s    %5.2f%%' % (candidate, s * 100))

        if printed:
            print()


def main():
    if not os.path.exists('features.db'):
        print('Features database not found')
        exit(0)
    
    if not os.path.exists('inverted.db'):
        generate_inv()
    
    if len(argv) > 1:
        if argv[1] == 'all':
            start_time = time.time()
            search_all_inv()
            print('Time spent: %s seconds' % (time.time() - start_time))
        else:
            for match in search_inv(argv[1]):
                print('%s    %5.2f%%' % (match[0], match[1] * 100))


if __name__ == '__main__':
    main()

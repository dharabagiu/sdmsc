SELECT ar AS row_num, bc AS col_num, SUM(prod) FROM (
        SELECT a.row_num AS ar, a.col_num AS ac, b.row_num AS br, b.col_num AS bc,
        a.value, b.value, a.value * b.value AS prod FROM a JOIN b WHERE ac = br
) GROUP BY ar, bc;

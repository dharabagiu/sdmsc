import sqlite3
import numpy as np
from sys import argv


def get_matrix(db, name, nrows, ncols):
    conn = None
    try:
        conn = sqlite3.connect(db)
    except sqlite3.Error as e:
        print(e)
        return None
    cur = conn.cursor()
    cur.execute('SELECT * FROM %s' % name)
    rows = cur.fetchall()
    m = np.zeros((nrows, ncols))
    for row in rows:
        m[row[0]][row[1]] = row[2]
    conn.close()
    return m


def main():
    if len(argv) < 2:
        exit()
    a = get_matrix(argv[1], 'a', 5, 5)
    b = get_matrix(argv[1], 'b', 5, 5)
    r = np.matmul(a, b)
    print('A =')
    print(a)
    print('\nB =')
    print(b)
    print('\nA*B =')
    print(r)


if __name__ == '__main__':
    main()

WITH Res AS (
	SELECT a.docid, SUM(a.count * b.count) AS sim FROM
	Frequency a JOIN (
		SELECT "q" as docid, "washington" as term, 1 as count
		UNION
		SELECT "q" as docid, "taxes" as term, 1 as count
		UNION
		SELECT "q" as docid, "treasury" as term, 1 as count
	) b ON (a.term = b.term) GROUP BY a.docid
) SELECT docid, sim FROM Res WHERE sim = (SELECT MAX(sim) FROM Res);
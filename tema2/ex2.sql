SELECT docid FROM (
	SELECT docid, SUM(count) AS total FROM Frequency GROUP BY docid
) WHERE total > 300;
